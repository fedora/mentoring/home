## Project Application

Why does your org want to participate in Google Summer of Code? (1000 chars)

    One of Fedora's four foundations is Friends. We highly value community and want to participate in Google Summer of Code to grow our pool of friends, both as users and contributors. We believe that Fedora, as an operating system, represents a platform for student education and experimentation. GSoC gives us the opportunity to formalize this in a way that leads to both better education for students and real world experience with projects and contributions to projects and their upstreams. We are a diverse community and understanding this will help students see how their education and goals relate to the larger world. Fedora is both an upstream and a downstream member of the Open Source community. Participation in our project allows students to see both sides of this equation. Additionally, using Fedora has positive return-on-investment for students because we are the upstream for Red Hat Enterprise Linux, the largest enterprise Linux and the likely platform of their future employers.

How many potential mentors have agreed to mentor this year? (range: 1-5, 6-10, 11-15, 16-20, 20+)

    6-10 (I don't think we can do more than this realistically)

How will you keep mentors engaged with their students? (1000 chars)

    Fedora has a Community Action and Impact Coordinator (FCAIC) who will be able to help in this regard. The FCAIC will maintain regular contact with mentors to ensure they are present and not overwhelmed. A mentor call with a light agenda will be scheduled periodically from the application period until the end of participation to provide an outlet for regular mentor to mentor contact. The topics presented will help our mentors grow as much as our students and provide a forum for solving problems. Additionally, Fedora is sponsored by Red Hat, a company with additional mentoring and educational resources we can draw on to help all of our participants succeed.

How will you help your students stay on schedule to complete their projects? (1000 chars)

    During the application process, mentors and students will work together to set realistic milestones and smaller goals for the project proposal. This timeline creates a structure that helps students produce regular deliverables and get early feedback. This method surfaces challenges or difficulties before they become stoppers. Additionally, students are asked to write weekly blog posts to share their progress on their projects. These will be read by their mentor(s), the Fedora Community Action and Impact Coordinator, and the Fedora community. The blog posts help students think critically about their own progress. When appropriate, writing prompts will be provided to guide thinking. These posts will also illuminate the progress being made toward the milestones defined at the start of the project. We will have periodic calls with students about their progress and provide them with a chance to share their own ideas and suggestions about their project and the overall program.

How will you get your students involved in your community during GSoC? (1000 chars)

    We use the community bonding period as a chance to onboard students into the project community. We introduce the students publicly on the Fedora Magazine as participants in this year's program and circulate this information widely across the project. We encourage students to participate in their project areas early in the process and not wait until the project period to begin engaging with the community or doing background reading and research. We hold meetings with all students and mentors to help encourage them to use each other as resources and support each other during the program. In previous years this has resulted in relationships that go beyond the length of GSoC. Students will also have the FCAIC and general mentors to help answer any non-project specific questions and to provide assistance. Lastly, our entire community is encouraged to participate on our GSoC mailing list to provide help and guidance to students even in an unofficial role.

How will you keep students involved with your community after GSoC? (1000 chars)

    In the last few weeks of GSoC, students are actively engaged with not only the mentor but the group their project is related to. This integrates them into the team surrounding their project and gives them visibility into other work happening in this team's area. Students are encouraged to attend the annual Fedora contributor conference, Flock, to give them an opportunity to meet and interact with their mentors and other community members. We will also work to connect them with local Fedora communities in their region. Students will also be featured in the Fedora Magazine articles about their project work. This effort not only promotes the work the student is doing, but also motivates them to continue participating in the community after their project has ended because they see the public recognition of their work.

Has your org been accepted as a mentor org in Google Summer of Code before?

    Yes

    Years of Participation 

    2005

    2006

    2007

    2008

    2009

    2010 - did not participate

    2011

    2012

    2013

    2014

    2015

    2016

    2017

    For each year provide the counts of successful and total students (e.g. 2016: 3/4) //It seems to be really hard to find out. Below are the only numbers i found out, and I do not think that these are all "successful..." . i don't think it matters that much as fedora is a well established project

    2005: 8 (6?) https://developers.google.com/open-source/gsoc/2005/

    2006: 5 https://developers.google.com/open-source/gsoc/2006/

    2007: 5 https://developers.google.com/open-source/gsoc/2007/

    2008: 10 (https://developers.google.com/open-source/gsoc/2008/)

    2009: 9 (8?) https://www.google-melange.com/archive/gsoc/2009/orgs/redhat

    2010: 0 - did not participate

    2011: 5 (https://www.google-melange.com/archive/gsoc/2011/orgs/fedora)

    2012: 9 (https://www.google-melange.com/archive/gsoc/2012/orgs/fedora)

    2013: 9 (https://www.google-melange.com/archive/gsoc/2013/orgs/fedora)

    2014: 8 (https://www.google-melange.com/archive/gsoc/2014/orgs/fedora)

    2015: 5 (https://www.google-melange.com/archive/gsoc/2015/orgs/fedora)

    2016: 6 (https://summerofcode.withgoogle.com/archive/2016/organizations/5911585470021632/) (per Kushal there were 10 and 6 passed)
    
    2017: 6 (5 successful)

    Submitted text: 2005: 8; 2006: 5; 2007: 5; 2008:10; 2009: 9; 2011: 5; 2012: 9; 2013: 9; 2014: 8; 2015: 5: 2016: 10/6; 2017 6/5 - data is approximated where not available.

If your org has applied for GSoC before but not been accepted, select the years

    none - assuming that no application was made in 2010

What year was your project started?

    2003

Where does your source code live?

    Various repositories and git hosts on the internet, however it is focused at pagure.io

Anything else we should know (500 chars)

    n/a


## Project Profile

Display Name

    Fedora Project

Website URL

    https://getfedora.org/

Tagline - A very short description of your project

    Examples: https://developers.google.com/open-source/gsoc/help/org-profile#descriptions

    Fedora Linux is built on the foundations of Freedom, Friends, Features, & First

Logo

    https://pagure.io/mentored-projects/blob/master/f/gsoc-notes/2018/fedora_gsoc_logo.png

Primary Open Source License

    MIT

Organization Category - Select which category fits your organization best. Some organizations may fit into multiple categories, you must choose one. Used to help students filter the organization list.

    Operating System

 Technology Tags - Enter keywords for the primary specific technologies your organization uses. Examples: Python, Javascript, MySQL, Hadoop, OpenGL, Arduino (max 5)
    // regarding tags, i'm not sure if ansible and postgresql really describe what fedora is about or what projects to expect however these are the technologies we use..  What would you like to replace them with? LDAP? JS?

    linux

    python

    git

    postgresql

    ansible

Topic Tags - Enter keywords for general topics that describe your organization. Examples: Vision, Robotics, Cloud, Graphics, Web, Real time (max 5)

    linux distribution

    desktop

    community

    web development

    server

Ideas List - Enter the URL of your Ideas List page. This will be linked from your organization page.

    https://docs.fedoraproject.org/mentored-projects/gsoc/2018/ideas.html

Short Description -- displayed on the organization list page (180 Chars)

    Examples: https://developers.google.com/open-source/gsoc/help/org-profile#descriptions

    Advance Free/Open Source Software/Content. Fedora is more than just one technology or concept it is a larger collaborative community.

Long Description - on your organization's page (2000 chars) - limited markdown: https://developers.google.com/open-source/gsoc/help/markdown

    Examples: https://developers.google.com/open-source/gsoc/help/org-profile#descriptions

The Fedora Project's core values, or Foundations, are Freedom, Friends, Features, & First. Read more about them here: https://fedoraproject.org/wiki/Foundations

A key component of this is our **Community**. This community, which you will join as an participant in Google Summer of Code, is creating a platform that supports the work of a diverse audience. Your contributions can affect people you've never met in situations you've never dreamed of. The Fedora community includes software engineers, artists, system administrators, web designers, writers, speakers, and translators -- all of whom will be happy to help you get started.

Full project description available here: https://fedoraproject.org/wiki/Overview

We believe that all contributors should expect and be part of a safe and friendly environment for constructive contribution. We can more effectively and successfully compare and challenge different ideas to find the best solutions for advancement, while building the size, diversity, and strength of our community.

Application Instructions - Guidance for students on how to apply to your organization. Should include any prerequisites or requirements. You may wish to include a template or tips for their proposals. May include limited Markdown. (1500 chars)

Our application process is described completely here: https://docs.fedoraproject.org/mentored-projects/gsoc/2018/application.html

In general, we are looking for students to describe both their understanding of the project they are choosing (or their complete project if they are proposing one) and to help us understand why they are the best candidate for the project. This also includes helping us understand their availability and level of commitment. While prior experience with Fedora is ideal, it is not a selection requirement.

Proposal Tags - Enter tags that students can select (one) from and apply to their own proposals to help organize them. Examples: New Feature, Optimization. You can also use these to designate "sub-organizations" if you are an umbrella organization. (max 10)

    Continuous Integration/Deployment (CI/CD)

    Development:Python

    Development:Web / App development

    Metrics

    Web Services

    Messaging

    Infrastructure:Authentication/FAS/krb5

    Development:General

    Community

    Propose Your Own Idea

You must complete at least one of the following three contact options.

Chat

     Freenode IRC Channel #fedora-summer-coding - https://webchat.freenode.net/?channels=#fedora-summer-coding

Mailing List

     summer-coding@lists.fedoraproject.org - https://lists.fedoraproject.org/admin/lists/summer-coding@lists.fedoraproject.org

General Email

     summer-coding@lists.fedoraproject.org

Links (all optional)

Google+

     https://plus.google.com/+Fedora

Twitter

     https://twitter.com/fedora

Blog URL

     https://communityblog.fedoraproject.org


