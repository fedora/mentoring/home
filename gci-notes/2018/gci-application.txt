## Project Application

Why does your organization want to participate in Google Code-in 2018? (500 chars)

    One of Fedora's four foundations is Friends. We highly value community and want to participate in Google Code In to introduce new people to the world of Open Source. We believe that Fedora is a platform for student education and experimentation. We are a diverse community and understanding this will help students see how their education and goals relate to the larger world. Fedora is allows students to experience both being part of an upstream provider and a downstream consumer of Open Source.

How has your organization prepared for Google Code-in 2018? (500 chars)

    Our Organization has been helping mentors figure out bite-sized tasks for GCI 2018. Mentors have been in touch since Flock 2018, our Annual Contributor Conference, and have spent time to create scopes inside the Fedora subprojects to acomodate students willing to contribute in different aspects like coding, documentation, design, outreach and testing. We are also working with select subprojects to provide additional tasks and mentoring for those tasks if we are selected.

Has your organization participated in Google Code-in before?

    Yes

    Years of Participation

    2012

Has your organization participated in Google Summer of Code before?

    Yes

    Years of Participation 

    2005

    2006

    2007

    2008

    2009

    2010 - did not participate

    2011

    2012

    2013

    2014

    2015

    2016

    2017

    2018

How many mentors have committed to participate? 

    6 put on form
    3 +2 backup mentors + additional mentors from subprojects contributing tasks

How do you plan to deal with any holidays or vacations mentors may have planned during the contest period? (500 chars)

    Holidays and vacations are able to be covered because the Organizational Administrators are also serving as backup mentors. We selected this process to ensure we had focus on both the mentor and administrative roles, but we also recognized the need for cross-functional backups. Our org admins are able to either perform the mentoring directly, or in the case of areas where they are not experts locate additional community members to help during mentor absences.

How do you plan to deal with unresponsive mentors? (500 chars)

    Fedora has a Community Action and Impact Coordinator (FCAIC) who will be able to help in this regard. The FCAIC will maintain regular contact with mentors to ensure they are present and not overwhelmed. Our mentors are all experienced with Google Summer of Code and have all worked together extensively. They know and support each other. Additionally, Fedora is sponsored by Red Hat, which has additional mentoring and educational resources we can draw on to help all of our participants succeed.

Example Tasks (25) URL: 

## Project Profile

Name

    Fedora Project

Website URL

    https://getfedora.org/

Short Description (200 chars)

        Advance Free/Open Source Software/Content. Fedora is more than just one technology or concept it is a larger collaborative community.

Long Description (1500 chars)

The Fedora Project is a community of people working together to build a free and open source software platform and to collaborate on and share user-focused solutions built on that platform. Or, in plain English, we make an operating system and we make it easy for you do useful stuff with it.

A key component of this is our **Community**. This community, which you will join as an participant in Google Code In, is creating a platform that supports the work of a diverse audience. Your contributions can affect people you've never met in situations you've never dreamed of. The Fedora community includes software engineers, artists, system administrators, web designers, writers, speakers, and translators -- all of whom will be happy to help you get started.

Full project description available here: https://docs.fedoraproject.org/en-US/project/

We believe that all contributors should expect and be part of a safe and friendly environment for constructive contribution. We can more effectively and successfully compare and challenge different ideas to find the best solutions for advancement, while building the size, diversity, and strength of our community.

Tags (5)

    distribution

    operating system

Programming Languages and Technologies  (5)

    linux

    python

    git

    postgresql

    ansible

Primary Open Source License

    MIT

Contact Methods
  Chat

    Freenode IRC Channel #fedora-summer-coding - https://webchat.freenode.net/?channels=#fedora-summer-coding


  Mailing List

     summer-coding@lists.fedoraproject.org - https://lists.fedoraproject.org/admin/lists/summer-coding@lists.fedoraproject.org
 
  Contact Email

     summer-coding@lists.fedoraproject.org
 
Google+

     https://plus.google.com/+Fedora

Twitter

     https://twitter.com/fedora

Blog URL

     https://communityblog.fedoraproject.org

Extra Task Information (250) (optional)

Common Task Information URL (optional)
 

