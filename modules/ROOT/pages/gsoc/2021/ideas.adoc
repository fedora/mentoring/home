

NOTE: Fedora is applying to be a GSoC mentoring organization.
////
NOTE: Fedora is proud to have been accepted as a GSoC mentoring organization.  Student applications open on March 25, 2019.  Please make sure you carefully read through the xref:gsoc/2019/index.adoc[general information] and xref:gsoc/2019/application.adoc[application process] pages before applying.
////
If you are a student looking forward to participating in
xref:gsoc/2021/index.adoc[Google Summer of Code with Fedora], please feel free to
browse this idea list.  There may be additional ideas added during the
application period.

**Now please go read the xref:gsoc/2021/index.adoc#what-can-i-do-today[What
Can I do Today] section of the main page. This has the answers to your
questions and tells you how to apply**

Do not hesitate to contact the mentors or contributors listed on this
page for any questions or clarification. You can find helpful people on
the IRC channel, or use the mailing list. can be used for getting help
with programming problems.

== Supporting Mentors

The following contributors are available to provide general help and
support for the GSoC program If a specific project mentor is busy, you
can contact one of the people below for short-term help on your project
or task.
add yourselves and your wiki page).


* link:https://fedoraproject.org/wiki/User:Sumantrom[Sumantro Mukherjee] (General development, general Linux,Fedora community, GSoC alumnus, questions about program, misc. advice)
* link:https://fedoraproject.org/wiki/User:Siddharthvipul1[Vipul Siddharth] (Fedora CI,GCI,GSoC,general linux,Fedora community, misc.)

== Idea list

NOTE: Ideas are subject to change as additional mentors are onboarded.


* <<Fedora QA Dashboard>>
* <<KiwiTCMS integration with Fedora QA workflow>>


=== Fedora QA Dashboard

- Difficulty : Intermediate
- Technology : Javascript, React, HTML5, CSS3
- Mentor :  Lukas Brabec , Josef 
- Email : lbrabec@redhat.com, jskladan@redhat.com (as backup)


==== Description

Fedora QA dashboard is an idea of a web application that would be the landing page for QA related activities. The current state, as we see it, is that we have tons of helpful documents, tools, and processes, but these are scattered, or sometimes hard to reach/understand without some preliminary knowledge. We identified this as the major obstacle in bootstrapping new members of the community. Goal for this project is making the learning curve less steep. We have basic PoC, that could serve as a base to build from, or an inspiration for a rework.

==== Deliverables
As a GSoC intern, you will be responsible for the following :

- Code of single-page application in javascript
- Buildable dockerfile, so the app can be deployed in our openshift cluster
- Share monthly reports to the community blog and QA meetings

'''
=== KiwiTCMS integration with Fedora QA workflow

- Difficulty : Intermediate
- Technology : Python, Container basics, JSON-RPC API
- Mentor :  Alex Todorov (Kiwi TCMS project lead),Sumantro Mukherjee (QA team)
- Email : atodorov@otb.bg, sumukher@redhat.com


==== Description

Kiwi TCMS has been in talks for Fedora QA since sometime. Last year, Fedora team members made a POC run to a KiwiTCMS instance for sometime. However, the whole idea was to run it and test the viability of Kiwi TCMS becoming a default TCMS for Fedora.
As a initial part of the POC efforts, QA identified a few missing pieces which become obstruction to having Kiwi TCMS as our go to TCMS.

- Develop and integrate plugin for FAS/AAA AKA noggin for Authentication
- Integrate bugzilla with the workflow 
- Add functionality for submission of anonymous result submission

==== Deliverables
As a GSoC intern, you will be responsible for the following :

- Finishing the Authentication plugin 
- Build, test and deploy the TCMS and update as community reviews  
- Share monthly reports to the community blog and QA meetings


'''
