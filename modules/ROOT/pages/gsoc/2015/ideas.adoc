= Ideas: Google Summer of Code 2015
Find an idea you like? Want to propose your own? See the
link:student_application_process.html[GSoC Getting Started Guide].

Further, last year accepted ideas from the Fedora Project can be found
at http://www.google-melange.com/gsoc/org/google/gsoc2014/fedora[GSoC
2014 web site]

[[students-welcome]]
== Students Welcome

If you are a student looking forward to participate the GSoC 2016 with
Fedora, please feel free to browse the idea list which is still growing.
Do not hesitate to contact the mentors/ contributors as indicated in
this page for any related clarification. You also should find some
like-minded people on the `#fedora-summer-coding` IRC channel.

If you are new to The Fedora project, the following material would help
you to get started. Further please sign-up with the link:https://fedoraproject.org/wiki/FAS[Fedora
Account System(FAS)] if you are willing to continue with the Fedora
project. `#fedora-devel`, IRC channel can be used to get instant
support.

1.  link:https://fedoraproject.org/wiki/Foundation[The Foundation]
2.  http://docs.fedoraproject.org/en-US/index.html[Fedora Documentation
    (Users/ Contributors)]
3.  link:https://fedoraproject.org/wiki/Communicate/IRCHowTo[How to work with IRC?]
    https://fedoramagazine.org/begginers-guide-to-irc[Beginner's Guide to
    IRC]
4.  link:https://fedoraproject.org/wiki/FAS[Fedora Account System]
5.  link:https://fedoraproject.org/wiki/Development[Development]

[[supporting-mentors]]
== Supporting Mentors

Following contributors are also willing to support the GSoC 2016
program. (please feel free to add your self, attach the user page).
Sometimes there should be some backing up mentors to mentor if the
original mentor get busy with something for a short time period. In such
case we need help.

1.  link:https://fedoraproject.org/wiki/User:Sgallagh[Stephen Gallagher]
2.  link:https://fedoraproject.org/wiki/User:Lsd[Lali Devamanthri]
3.  link:https://fedoraproject.org/wiki/User:corey84[Corey Sheldon] (linuxmodder)

[[draft-of-an-idea]]
== Draft of an idea

Please add your idea as follows.

[[project-name]]
=== Project name

_Status:_

_Summary of idea:_

_Knowledge prerequisite:_

_Skill level:_

_Contacts:_

_Mentor(s):_

_Notes:_

*!!!The draft was changed slightly, please add required field as
required!!!*

[[idea-list-for-gsoc-2016]]
== Idea list for GSoC 2016

[[implement-tinykdump]]
=== Implement Tinykdump

_Status:_ Proposed - draft

_Summary of idea:_ Tinykdump is a minimal daemon to capture kernel-based
crash dumping (kdump) memory image to usb storage. Compared to the
traditional kdump solution, it is,

`* more reliable and scalable` +
`* has smaller memory foot-print` +
`* more friendly to kernel developers `

More information here: https://fedorahosted.org/tinykdump/

_Knowledge prerequisite:_ Python, kernel programming (desired)

_Skill level:_ intermediate (programming)

_Contacts:_ link:https://fedoraproject.org/wiki/User:caiqian[CAI Qian]

_Mentor(s):_ link:https://fedoraproject.org/wiki/User:caiqian[CAI Qian]

_Notes:_ Rough roadmap:

* Implement tinykdump daemon to be included in Fedora.
* Submit kernel patches for reserving kdump memory at run-time for
  community review and inclusion.
* Currently, pstore only log kernel messages for panic and Oops. Patches
  are needed to support logging of kdump kernel and initramfs console
  output.

[[improve-fedora-review]]
=== Improve Fedora Review

_Status:_ Proposed - draft

_Summary of idea:_ Every package that is included into Fedora needs to
go through a review to ensure it matches the Packaging:Guidelines.
https://fedorahosted.org/FedoraReview/[Fedora Review] is a tool to help
with the review. It needs constant development to be updated for changes
in the guidelines. Also there is currently no process to ensure that
existing packages adhere to the packaging guidelines. This project is
meant to improve this.

_Knowledge prerequisite:_ Python 2&3 programming skills

_Skill level:_ intermediate (programming)

_Contacts:_ link:https://fedoraproject.org/wiki/User:Leamas[Alec Leamas], link:https://fedoraproject.org/wiki/User:Till[Till Maas]

_Mentor(s):_ link:https://fedoraproject.org/wiki/User:Till[Till Maas]

_Notes:_ Possible tasks:

* Make Fedora Review PEP8 compliant, fix its current test cases
* Help running Fedora Review regularly for existing packages e.g.,
  updating the Jenkins continuous builds and/or integrate it into
  Taskotron
* Add static code checker support to Fedora Review (e.g. with
  https://git.fedorahosted.org/git/csmock.git[csmock])
* Build a web service mockup supporting the review process
  http://fedoraproject.org/wiki/Package_Review_Process[1] replacing
  current bugzilla workflow.

[[enhance-fedora-build-setup]]
=== Enhance Fedora build setup

_Status:_ Proposed - draft

_Summary of idea:_ Fedora uses the http://koji.fedoraproject.org[Koji]
build system with GIT as a source. Plugings/scripts are needed to allow
package maintainer to be able to use own branches without accidently
deleting the source of published RPMs.

_Knowledge prerequisite:_ Python programming skills, ideally some
packaging knowledge

_Skill level:_ intermediate (programming), high (understanding/analysing
code, GNU/Linux)

_Contacts:_ link:https://fedoraproject.org/wiki/User:Till[Till Maas], link:https://fedoraproject.org/wiki/User:Ausil[Dennis Gilmore],

__Mentor(s):__link:https://fedoraproject.org/wiki/User:Till[Till Maas], link:https://fedoraproject.org/wiki/User:Ausil[Dennis Gilmore]

_Notes:_ Rough roadmap:

* Make select https://fedorahosted.org/rel-eng/[releng scripts] PEP8
  compliant/python3 ready
* Make other python tools PEP8 compliant, python3 ready:
** https://fedorahosted.org/fedpkg/[fedpkg]
** https://git.fedorahosted.org/cgit/mash/[mash]
** https://fedorahosted.org/rpkg/[rpkg]
* Become familiar with the Fedora packaging workflow, maybe by packaging
  some software
* Learn how to interface koji and write a script to get a mapping of git
  commit ID to package build (name, version, release)
* Write a koji plugin to enforce that pkgs can be only built from the
  right GIT branch for each build target (might need improvements to
  koji's plugin interface as well):
  https://fedorahosted.org/rel-eng/ticket/5843
* Write a fedmsg service/cronjob to regularly tag sucessful builds in
  GIT: https://fedorahosted.org/rel-eng/ticket/5856
* Help with koji2

[[improve-sigul-signing-server]]
=== Improve Sigul Signing Server

_Status:_ Proposed - draft

_Summary of idea:_ The https://fedorahosted.org/sigul/[Sigul] signing
server is used by release engineering to
link:https://fedoraproject.org/wiki/Release_package_signing[sign Fedora RPMs] when
link:https://fedoraproject.org/wiki/pushing_fedora_updates[pushing fedora updates]. There are two major
problems that make it hard to release updated packages in a timely
manner: It crashes and it cannot be used simultaneously.

_Knowledge prerequisite:_ Python programming skills, ideally some
background knowledge about GPG, security and networking

_Skill level:_ intermediate (programming), high (understanding/analysing
code, GNU/Linux)

_Contacts:_ link:https://fedoraproject.org/wiki/User:Till[Till Maas], link:https://fedoraproject.org/wiki/User:Ausil[Dennis Gilmore],
link:https://fedoraproject.org/wiki/User:ralph[Ralph Bean],

_Mentor(s):_ link:https://fedoraproject.org/wiki/User:Till[Till Maas], link:https://fedoraproject.org/wiki/User:Ausil[Dennis Gilmore],
link:https://fedoraproject.org/wiki/User:ralph[Ralph Bean]

_Notes:_ Rough roadmap:

* To test whether everything works, a test instance needs to be setup.
  This is rather complex because it requires interaction with koji. Maybe
  it is possible to add a test instance to Infrastructure that can use the
  koji staging system, but the latter is not fully functional right now.
* Debug why sigul hangs sometimes when using the
  https://git.fedorahosted.org/cgit/sigul.git/tree/src/client.py#n1090[sign-rpms]
  command (called by `--batch-size` greater than one with
  `sigulsign_unsigned.py`
* Enable sigul to process multiple tasks at once, e.g. sign for multiple
  releases or architectures at once.
* Fix other bugs/issues, examples:
** Currently http://linux.die.net/man/8/logrotate[logrotate] does not
   make sigul properly re-open its logfiles, which is why sigul does not
   log to the new logfile after rotation. This needs to be fixed in sigul
** The GPG defaults in sigul might not be up-to-date, they should be
   reviewed and improved if necessary
** Add support for e.g. signing and revoking GPG keys, to build a local
   web of trust between https://getfedora.org/keys/[Fedora release keys]

Ressources:

* https://git.fedorahosted.org/cgit/releng/tree/scripts/sigulsign_unsigned.py[Script
  used by rel-eng to run sigul]
* https://git.fedorahosted.org/cgit/sigul.git/tree/doc/protocol-design.txt[Protocol
  description]

[[askfedora-uxui-functionality-overhaul]]
=== AskFedora UX/UI & Functionality Overhaul

_Status:_ Proposed - draft

_Summary of idea:_

https://ask.fedoraproject.org/[AskFedora] is a community knowledge base
and support forum and designed to be the primary place for community
support in Fedora. It is powered by Askbot, Django based web
application. The UI and the UX for AskFedora needs overhaul to give it
some uniformity with the current Fedora websites. There may also be
changes to be done in Askbot itself and have possibility of being
integrated upstream. We aim to achieve results similar to what
http://askubuntu.com/[Ask Ubuntu] has achieved, however Ask Ubuntu is
not based on Askbot and similar theming techniques can't be applied.
Discussions are open for this.

'' But why?: ''Over the years of its existence, AskFedora's popularity
has increased and there are 11,000+ questions that have been asked on
the website and has 12,500+ contributors as of today (out of which quite
a few are active). We think, it really needs to 'look good' and 'provide
a better user experience' now.

''Status right now: '' Mockups during the last Design Fedora Activity
Day (FAD) 2015 were done. Checkout
https://suchakra.wordpress.com/2015/01/20/ask-fedora-ux-redesign-updates-1/[this]
and
https://suchakra.wordpress.com/2015/01/21/ask-fedora-ux-redesign-updates-2/[this]
blogpost for latest updates on mockups. An
http://askbotstg-suchakra.rhcloud.com/questions/[openshift instance] has
also been created and source for testing
https://github.com/fedoradesign/askbot-test[repository] is available for
setting up your own staging instance.

_Knowledge prerequisites:_ Front-end (HTML/CSS/JS) development, UI/UX
design experience, some knowledge of Django/Python

_Skill level:_ Beginner/Intermediate

_Contacts:_ kushal at fedoraproject dot org, suchakra at fedoraproject
dot org

_Mentor(s):_ link:https://fedoraproject.org/wiki/User:Kushal[ Kushal Das], link:https://fedoraproject.org/wiki/User:Suchakra[ Suchakra]

_co-Mentor(s):_ link:https://fedoraproject.org/wiki/User:Sarupbanskota[ Sarup Banskota]

TL;DR The infra and some ideas for testing is all ready, we need someone
to improve AskFedora's UI/UX.

[[glitter-gallery-improvements]]
=== Glitter Gallery Improvements

_Status:_ Proposed - draft

_Summary of idea:_

link:https://fedoraproject.org/wiki/Design/GlitterGallery[ GlitterGallery] is GitHub for designers -
being developed by and for the Fedora design team, but hoping to be
useful to all designers. It's a web app that allows designers and
artists to create, share, and collaborate, backed by Git for version
control, and intended to be part of a FLOSS design suite that includes

* http://sparkleshare.org[Sparkleshare] - a git-backed, Dropbox like
  system that will automatically check in and push files in project
  directly to a shared git repo
* https://github.com/garrett/magicmockup[Magic Mockup] - a javascript
  library you can insert into an SVG of mockups to enable interactive,
  click-through mockups
  (http://blog.linuxgrrl.com/2011/08/12/interactive-svg-mockups-with-inkscape-javascript/[see
  a demo here]
* http://inkscape.org[Inkscape] is our preferred design tool of choice

Last year, two GSoC students worked on a number of critical improvements
to GlitterGallery, but there is still plenty of work to be done.

* Public gallery of works; currently the app requires a user to login
  and to follow other users before they can see work other than their own.
  They can also view direct links to works. A public gallery can be used
  to browse and explore works without having to be logged in.
* Better design suite integration, which could mean better support for
  local editing with SparkleShare; Inkscape integration through an
  extension; and/or support for creating and sharing interactive SVGs with
  Magic Mockup
* Better commenting - the current commenting system is basic, and
  there's lots of ways it could be improved, including thread support,
  pingback support, the ability to reference a specific region of a design
  in a comment
* External issue tracking - Glitter Gallery has an integrated issue
  tracker, but it would be useful to also be able to integrate with
  external bug/issue trackers such as GitHub and Bugzilla.
* Enhanced history view - (see
  https://github.com/glittergallery/GlitterGallery/issues/187)
* Your own ideas

_Knowledge prerequisites:_ git, Ruby on Rails, front-end (HTML/CSS/JS)
development, design experience would be great but optional

_Skill level:_ Intermediate

_Contacts:_ emichan at fedoraproject dot org, sarupbanskota at
fedoraproject dot org

_Mentor(s):_ link:https://fedoraproject.org/wiki/User:Emichan[ Emily Dirsh], link:https://fedoraproject.org/wiki/User:Sarupbanskota[ Sarup
Banskota], link:https://fedoraproject.org/wiki/User:_Rohitpaulk[ Rohit Paul Kuruvilla]

_Notes:_ The
https://github.com/glittergallery/GlitterGallery[GlitterGallery
repository] is hosted on GitHub.

[[multimonitor-wallpaper-submission-and-download-for-nuancier]]
=== Multimonitor wallpaper submission and download for Nuancier

_Status:_ Proposed

_Summary of idea:_ Purpose for this project is to extend
https://apps.fedoraproject.org/nuancier/[Nuancier], Fedoras supplemental
wallpaper submission application with an system that allow to submit and
downlad wallpapers for multi-monitor setup.

Knowledge prerequisite: some Python knowledge

_Skill level:_ starter

_Contacts:_ Sirko Kemter gnokii@fedoraproject.org

_Mentor(s):_ link:https://fedoraproject.org/wiki/User:Pingou[ Pierre-Yves Chibon], link:https://fedoraproject.org/wiki/User:Gnokii[ Sirko
Kemter]

[[cockpit-ui-for-rolekit]]
=== Cockpit UI for Rolekit

_Status:_ Proposed _Summary of idea:_ The
https://fedorahosted.org/rolekit[Rolekit Project] provides a platform
API for deploying Server Roles onto a system. Currently, it supports
creating a Domain Controller (based on http://freeipa.org[FreeIPA]) or a
Database Server (based on http://www.postgresql.org/[PostgreSQL]). A
major component of the Fedora Server is the
http://cockpit-project.org[Cockpit Project], a web-based management
console for servers. The goal of this effort would be to enhance the
Cockpit UI so that an administrator could deploy these Roles using
rolekit via the Cockpit web interface.

_Knowledge prerequisites:_ JavaScript (ideally jQuery). Preferred
familiarity with D-BUS.

_Skill Level:_ Beginner to intermediate

_Contacts:_ link:https://fedoraproject.org/wiki/User:Sgallagh[Stephen Gallagher]

_Mentor(s):_ link:https://fedoraproject.org/wiki/User:Sgallagh[Stephen Gallagher]

_Success Conditions:_ A user of the Cockpit UI must be able to deploy a
Domain Controller while providing the minimum set of necessary
information to the UI. The UI must optionally allow more advanced
settings to be selected. The UI must also provide a link post-deployment
that allows the user to browse to the Domain Controller administration
UI. Providing the same functionality for the Database Server role would
be bonus functionality for this project, provided that the Domain
Controller is completed early.

[[cockpit-support-for-systemd-timers]]
=== Cockpit support for systemd timers

_Status_: Proposed Summary of idea: systemd provides timers for calendar
time events and monotonic time events
(http://www.freedesktop.org/software/systemd/man/systemd.timer.html,
https://wiki.archlinux.org/index.php/Systemd/Timers). A major component
of the Fedora Server is the Cockpit Project, a web-based management
console for servers. The goal of this effort would be to enhance the
Cockpit UI so that an administrator could deploy these Roles using
rolekit via the Cockpit web interface. Some preliminary designs for
timers in Cockpit exist at
https://trello.com/c/1B2lZViZ/74-timers-and-cron, although these are
just intended as guidelines to get started.

_Knowledge prerequisites_: JavaScript (ideally jQuery). Preferred
familiarity with D-BUS.

_Skill Level_: Beginner to intermediate

_Contacts_: link:https://fedoraproject.org/wiki/User:Sgallagh[Stephen Gallagher]

_Mentor(s)_: Dominik Perpeet, link:https://fedoraproject.org/wiki/User:Sgallagh[Stephen Gallagher]

_Success Conditions_: A user of the Cockpit UI must be able to view
existing timers, edit existing ones or create new timers while providing
the minimum set of necessary information to the UI. The UI must
optionally allow more advanced settings to be selected.

[[docker-volume-support-in-cockpit]]
=== Docker Volume Support in Cockpit

_Status_: Proposed Summary of idea: Docker (https://www.docker.com/)
allows building, running, and sharing of software in containers. A major
component of the Fedora Server is the Cockpit Project, a web-based
management console for servers. Docker is already supported in Cockpit,
including features such as downloading images, running/stopping/deleting
containers, exposing additional ports, inspecting console output as well
as linking containers. The goal of this effort would be to enhance the
Cockpit UI so that an administrator can use Docker 'Data volumes'
(http://docs.docker.com/userguide/dockervolumes/) via the Cockpit web
interface. Some preliminary designs for Docker Volume Support in Cockpit
exist at https://trello.com/c/4juwxCaE/94-docker-volume-support,
although these are just intended as guidelines to get started.

_Knowledge prerequisites_: JavaScript (ideally jQuery). Preferred
familiarity with D-BUS.

_Skill Level_: Beginner to intermediate

_Contacts_: link:https://fedoraproject.org/wiki/User:Sgallagh[Stephen Gallagher]

_Mentor(s)_: Dominik Perpeet, link:https://fedoraproject.org/wiki/User:Sgallagh[Stephen Gallagher]

_Success Conditions_: A user of the Cockpit UI must be able to create,
view and (to a sensible extent) edit Docker 'Data Volume Containers'
while providing the minimum set of necessary information to the UI. It
should also be possible to select host directories or host files as Data
Volumes. The UI must optionally allow more advanced settings to be
selected. Optionally this can be extended by further functionality, such
as backup, restoration or migration of data volumes.

[[shumgreppersummershum]]
=== Shumgrepper/summershum

_Status:_ Proposed

_Summary of idea:_ Finish and deploy the shumgrepper project

_Knowledge prerequisite:_ python, flask, sqlalchemy and some system
administration

_Skill level:_ Intermediate

_Contacts:_ link:https://fedoraproject.org/wiki/User:pingou[Pierre-Yves Chibon]

_Mentor(s):_ link:https://fedoraproject.org/wiki/User:pingou[Pierre-Yves Chibon] link:https://fedoraproject.org/wiki/User:ralph[Ralph Bean]

_Notes:_ shumgrepper was started last year and offers an API to query
the data stored by summershum. This data corresponds to the md5, sha1,
sh256 and sha512 of every files in every packages in Fedora, allowing to
easily find out files duplicated in multiple packages.

Dev instance: http://209.132.184.120/

[[fresque]]
=== fresque

_Status:_ Proposed

_Summary of idea:_ Fedora Review Server: take package reviews off
bugzilla

_Knowledge prerequisite:_ python, flask, sqlalchemy and some ideas about
packaging

_Skill level:_ Intermediate

_Contacts:_ link:https://fedoraproject.org/wiki/User:abompard[Aurélien Bompard]

_Mentor(s):_ link:https://fedoraproject.org/wiki/User:abompard[Aurélien Bompard]

_Notes:_ fresque aims at providing a dedicated application for package
(RPM) reviews. This would be integrate with a git backend and integrates
the fedora-review tool for automatic testing of new reviews and changes.

[[patch-tracker]]
=== Patch Tracker

_Status:_ Proposed

_Summary of idea:_ One of Fedoras goals as a distribution is
link:https://fedoraproject.org/wiki/staying_close_to_upstream_projects[staying close to upstream
projects]. However, sometimes it is necessary for Fedora packages to
deviate from upstream, for example if upstream is dead or if a fix is
backported to an older release. Also other distributions sometimes carry
patches that are not submitted upstream but fix bugs also present in
Fedora packages. Therefore it is interesting for Fedora packagers or
packagers of other distributions to get easy access to information about
patches. Fedora already contains a web app, that shows information about
patches in Fedora, for example the
https://apps.fedoraproject.org/packages/ipython/sources/[patches for the
ipython package]. However, this is not a designated app to make the
patch information as useful as possible and does not contain support for
other distros. Debian used to provide a
https://anonscm.debian.org/cgit/users/seanius/patch-tracker.git/tree/[patch
tracker] that is currently offline due to a missing maintainer. For
other distributions, there only
http://oss-security.openwall.org/wiki/distro-patches[manual methods] to
find out about patches. Therefore the idea is to create a web
application with the purpose to make it easier for others to find
patches in Fedora and to make it easier for Fedora maintainers to find
patches in other distributions. FedoraCreate/adjust a webapp to track
patches that are used in Fedora and other distributions

_Knowledge prerequisite:_ Python programming, web application
development

_Skill level:_ Intermediate

_Contacts:_ link:https://fedoraproject.org/wiki/User:till[Till Maas]

_Mentor(s):_ link:https://fedoraproject.org/wiki/User:till[Till Maas]

_Notes:_ Potential features:

* Show a clear overview for patches in Fedora for a certain package
** Link to bugs that were mentioned, extract key information from the
   bug
* Allow to get notifications for new patches, e.g. via fedmsg
* Allow to get information about patches for the package in other
  distros
* Try to figure out if patches are already upstream
* ...

Rough potential roadmap:

* Get the debian patch tracker running on a test system, maybe with some
  example debian packages
* Port it for one example Fedora package
* Port it to a modern web framework such as Flask or Pyramid
* Make sure it is PEP8 compliant
* Make it generic to work for Fedora and Debian
* Add feature to patches that are present only in Fedora or Debian
* Add more distros, e.g. OpenSUSE, Arch, Ubuntu, Gentoo
* Add more features

Basic requirements:

* Target platform is RHEL/CentOS7 with EPEL
* All dependencies should be available on the target platform as RPM
  packages or possible to be packaged (e.g. requiring newer versions of
  packages already included in the target platform might not be easily
  possible)
* It needs to be possible to package the final project for Fedora/EPEL,
  i.e. there may not be bundled libraries included
* The code needs to be PEP8 compliant and contain proper docstrings
* Proper automtatic tests should be included to allow meaningful
  continuous integration

Recommended basic knowledge:

* Know about PEP8, pylint, continuous integration,
* Understand the different diff formats

[[better-oval-development-tools-in-openscap]]
=== Better OVAL development tools in OpenSCAP

_Status:_ Proposed

_Summary of idea:_ The https://www.open-scap.org[OpenSCAP] project
implements the Security Content Automation Protocol standards. It
provides users with a way to automatically audit their infrastructure.
One of the standards is OVAL - Open Vulnerability and Assessment
Language - it is the language in which the automated checks are written.
Unfortunately the check authors have it tough right now. They have to
edit XML files manually, there is no debugger, no static analysis of any
sort (like pylint or cppcheck). To make matters worse the OVAL checks
are hard to write and the learning curve is steep.

_Knowledge prerequisites:_ Intermediate C, familiarity with the OpenSCAP
project

_Skill Level:_ Intermediate

_Contacts:_ link:https://fedoraproject.org/wiki/User:Mpreisle[Martin Preisler]

_Mentor(s):_ link:https://fedoraproject.org/wiki/User:Mpreisle[Martin Preisler]

_Success Conditions:_ SCAP content developers are able to interactively
debug their checks, they can browse the execution, looking at inputs and
outputs of each step. They are able to analyze their OVAL content for
common mistakes using the lint-like tool. Such common mistakes include
ID mismatches, wrong usage of regexes, ...

[[fix-bugs-in-packages-that-break-compiling-as-position-independent-executable]]
=== Fix bugs in packages that break compiling as position independent
executable

_Status:_ proposed

_Summary of idea:_ Starting with Fedora 23, Fedora will try to ship all
binaries as
http://en.wikipedia.org/wiki/Position-independent_code[position
independent executable] to benefit from the
http://en.wikipedia.org/wiki/Address_space_layout_randomization[address
space layout randomization (ASLR)] of the Linux kernel to increase the
security of Fedoras packages. However, not all code/packages are ready
for this, therefore the
link:https://fedoraproject.org/wiki/Changes/Harden_all_packages_with_position-independent_code[current
change proposal] plans to handle these cases by just disabling this
improvement for affected packages. Ideally all packages would be fixed
to not require it to be disabled, this is where you can help.

_Knowledge prerequisite:_ low-level programming skills, compiler
development, build systems configuration, maybe python programming
skills

_Skill level:_ intermediate to high, depending on the actual problem

_Contacts:_ link:https://fedoraproject.org/wiki/User:Orion[Orion Poplawski], link:https://fedoraproject.org/wiki/User:till[Till Maas],
link:https://fedoraproject.org/wiki/User:halfie[Dhiru Kholia],

_Mentor(s):_ link:https://fedoraproject.org/wiki/User:Orion[Orion Poplawski], link:https://fedoraproject.org/wiki/User:till[Till Maas],
link:https://fedoraproject.org/wiki/User:halfie[Dhiru Kholia]

_Notes:_ Currently not all Fedora packages were rebuilt with the new
hardening flags, therefore it is not yet clear how many packages need to
be fixed. However, it might be that there are also some languages that
need to be adjusted in general to produce position-independent code (for
example go), so adding support to (some) of these languages would be in
the scope of this project as well, after all build failures were
handled. Additionally Fedora tools like
https://fedorahosted.org/FedoraReview/[Fedora Review] and Taskotron
should be adapted to properly report packages that are not built with
the hardening flags for example by running checkseck on all created
executables.

Example problems that block some packages to be built with the right
flags:

* https://bugzilla.redhat.com/show_bug.cgi?id=1199775

Helpful documentation:

* link:https://fedoraproject.org/wiki/Using_Mock_to_test_package_builds[Using Mock to test package
  builds]

[[enhance-postgresql-gssapi-support]]
=== Enhance PostgreSQL GSSAPI Support

_Status:_ proposed

_Summary of idea:_ The Fedora Server Edition includes a Database Server
Role powered by a PostgreSQL database. In order to integrate it better
with Fedora Server's Domain Controller, we want to enhance the GSSAPI
authentication and communication in the server.

_Knowledge prerequisite:_ C Programming, Kerberos/GSSAPI

_Skill level:_ intermediate to high (probably grad-student level)

_Contacts:_ link:https://fedoraproject.org/wiki/User:Sgallagh[Stephen Gallagher], link:https://fedoraproject.org/wiki/User:Simo[Simo Sorce]

_Mentor(s):_ link:https://fedoraproject.org/wiki/User:Simo[Simo Sorce]

_Notes:_ This will require modifications to the authentication subsystem
as well as the TCP/IP layer of PostgreSQL. It will likely be a very
involved project.

[[tweak-specific-packages-to-play-well-with-mips-architecture]]
=== Tweak specific packages to play well with MIPS architecture

_Status:_ proposed

_Summary of idea:_ We are planning to bootstrap Fedora for MIPS
architecture. Even though the majority of packages should build with
little to no modifications, there are some challenging packages that
will need extra attention.

_Knowledge prerequisite:_ C or Python Programming, packaging basics,
some idea about non-x86 architectures

_Skill level:_ intermediate to high

_Contacts:_ link:https://fedoraproject.org/wiki/User:Mtoman[Michal Toman], link:https://fedoraproject.org/wiki/User:Anibal[Anibal Monsalve
Salazar], link:https://fedoraproject.org/wiki/User:wzssyqa[YunQiang Su]

_Mentor(s):_ link:https://fedoraproject.org/wiki/User:Mtoman[Michal Toman]

_Notes:_ Possible work areas:

* kernel
* anaconda
* openjdk
* ghc
* ocaml
* erlang
* fpc
* ...

[[open-ideas-from-gsoc-2016]]
== Open Ideas From GSoC 2016

Despite the list of ideas, you may want to check out the ideas of the
previous years and contact the admins to see if they are still
interested in mentoring someone this year.
////
Previous years:

* link:Summer_coding_ideas_for_2015[2015]
* link:Summer_coding_ideas_for_2014[2014]
* link:Summer_coding_ideas_for_2013[2013]
* link:Summer_coding_ideas_for_2012[2012]
* link:Summer_coding_ideas_for_2011[2011]
* link:Summer_coding_ideas_for_2010[2010]
* link:Summer_coding_ideas_for_2009[2009]
* link:Summer_coding_ideas_for_2008[2008]

Please: Do not submit a proposal for an idea from a previous year
without previously contacting the admin to ensure their will to mentor
someone this year. Without mentor, proposals will be rejected.

Category:Summer_coding_2016[Category:Summer coding 2016]
////
